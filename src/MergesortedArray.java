import java.util.Arrays;

public class MergesortedArray {
    public static void main(String[] args) {
        int num1[] = {1,2,3,0,0,0}; int m = 3;
            int num2[] = {2,5,6}; int n = 3;
            System.out.println("Ex.1");
            System.out.println("input: nums1[] = {1,2,3,0,0,0} m = 3, nums2[] = {2,5,6} n = 3");
            for(int i=0;i<m+n;i++){
                if(i>m){
                    num1[i] = num2[i-m]; 
                }
                if(i==m){
                    num1[i] = num2[i-m]; 
                }
            }
            System.out.print("Output: ");
            Arrays.sort(num1);
            for(int num: num1){
                System.out.print(num+" ");
            }
            int num1_ex2[] = {1}; int m_ex2 = 1;
            int num2_ex2[] = {}; int n_ex2 = 0;
            System.out.println();
            System.out.println("Ex.2");
            System.out.println("input: nums1[] = {1} m = 1, nums2[] = {} n = 0");
            for(int i=0;i<m_ex2+n_ex2;i++){
                if(i>m_ex2){
                    num1_ex2[i] = num2_ex2[i-m]; 
                }
                if(i==m_ex2){
                    num1_ex2[i] = num2_ex2[i-m]; 
                }
            }
            System.out.print("Output: ");
            Arrays.sort(num1_ex2);
            for(int num: num1_ex2){
                System.out.print(num+" ");
            }
            int num1_ex3[] = {0}; int m_ex3 = 0;
            int num2_ex3[] = {1}; int n_ex3 = 1;
            System.out.println();
            System.out.println("Ex.3");
            System.out.println("input: nums1[] = {0} m = 0, nums2[] = {1} n = 1");
            for(int i=0;i<m_ex3+n_ex3;i++){
                if(i>m_ex3){
                    num1_ex3[i] = num2_ex3[i-m_ex3]; 
                }
                if(i==m_ex3){
                    num1_ex3[i] = num2_ex3[i-m_ex3]; 
                }
            }
            System.out.print("Output: ");
            Arrays.sort(num1_ex3);
            for(int num: num1_ex3){
                System.out.print(num+" ");

    }
}
}